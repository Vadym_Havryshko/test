<?php
function getSitemaps($startUrl, &$locations = []) {
    $filename = __DIR__.'/'.md5($startUrl).'.sitemap.xml';
    if(file_exists($filename) && false) {
        $content = @file_get_contents($filename);
    } else {
        $content = file_get_contents($startUrl);
        //file_put_contents($filename, $content);
    }
    $content = preg_replace('/(\>)\s*(\<)/m', '$1$2', $content);
    $subLinksExp = '/<sitemap><loc>(.+?)<\/loc>.+?<\/sitemap>/is';
    $linksExp = '/<url><loc>(.+?)<\/loc>(.+?)<\/url>/is';
    
    if (preg_match_all($subLinksExp, $content, $sitemaps)) {
        foreach ($sitemaps[1] as $subUrl) {
            getSitemaps($subUrl, $locations);
        }
    } elseif (preg_match_all($linksExp, $content, $urls)) {
        foreach ($urls[1] as $url) {
            $locations[] = parse_url($url, PHP_URL_PATH);
        }
    } else {
        echo 'Cannot load '.$filename.PHP_EOL;
    }
}

$locations = [];
getSitemaps('https://www.yaposhka.kh.ua/sitemap.xml', $locations);
$csv = implode(PHP_EOL, array_unique($locations));
file_put_contents(__DIR__ . '/yaposhka.csv', $csv);
